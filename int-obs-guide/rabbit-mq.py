#! /usr/bin/env python3

# https://pika.readthedocs.io/
# https://openbuildservice.org/help/manuals/obs-admin-guide/obs.cha.administration.html#id-1.6.7.10.3.4.6

import pika

creds = pika.PlainCredentials("suse", "suse")
params = pika.ConnectionParameters("rabbit.suse.de", 5672, "/", creds)
connection = pika.BlockingConnection(params)
channel = connection.channel()
channel.exchange_declare("pubsub", "topic", True, True)
result = channel.queue_declare(queue="", exclusive=True)
queue_name = result.method.queue
channel.queue_bind(queue_name, "pubsub", "#")

def callback(ch, method, properties, body):
    print("%s: %r)" % (method.routing_key, body))

channel.basic_consume(queue_name, on_message_callback=callback, auto_ack=True)
channel.start_consuming()
