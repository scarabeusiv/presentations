% The Open Build Service
% Tomáš Chvátal \<tchvatal@suse.com\>
% 2019-08-19

-------------------------------------------------------------------------------

# RabbitMQ Event Feed

- OBS distributes event notifications over the the Advanced Message
  Queuing Protocol (AMQP) of Rabbit MQ.

- Clients may subscribe at `rabbit.suse.de` using the login "`suse`", password
  "`suse`", and the virtual host "`/`".

- https://github.com/rabbitmq/rabbitmq-tutorials has example code written in
  Clojure, Common LISP, Dart, Dotnet, Elixir, Erlang, Go, Haskell, Java,
  Javascript Nodejs, Perl, PHP, Python, Ruby, Rust, Scala, and many others.

-------------------------------------------------------------------------------

# RabbitMQ Events generated by OBS for ...

## Packages
- `build_success`: a package build has succeeded
- `build_fail`: a package build has failed
- `comment`: a new comment for the package was created
- `upload`: sources of a package were uploaded

## Projects
- `create`: a new project was created
- `delete`: a project was deleted
- `update`: a project was updated
- `comment`: a new comment for the project was created

-------------------------------------------------------------------------------

# RabbitMQ Events generated by OBS for ...

## Requests
- `create`: a request was created
- `delete`: a request was deleted
- `review_wanted`: a request requires a review
- `comment`: a new comment for the request was created

-------------------------------------------------------------------------------

# Listening to RabbitMQ in Python

~~~ python
import pika

cred    = pika.PlainCredentials("suse", "suse")
param   = pika.ConnectionParameters("rabbit.suse.de", 5672, "/", cred)
channel = pika.BlockingConnection(param).channel()
channel.exchange_declare("pubsub", "topic", True, True)
result  = channel.queue_declare(queue="", exclusive=True)
qname   = result.method.queue
channel.queue_bind(qname, "pubsub", "#")

# User-provided callback function  to process incoming messages ...
def callback(ch, method, properties, body):
    print("%s: %r)" % (method.routing_key, body))

channel.basic_consume( qname, on_message_callback=callback
                     , auto_ack=True
                     )
channel.start_consuming()
~~~

-------------------------------------------------------------------------------

# Routing Keys

- Routing keys like identify the type of the message. A complete list is at
<https://openbuildservice.org/help/manuals/obs-admin-guide/obs.cha.administration.html#_rabbit_mq>.

- All routing keys from `build.suse.de` have the prefix "`suse.obs`". We
  sometimes receive events from "`opensuse`", too, but those should be ignored.

- Every routing key (message type) comes with a dictionary of additional
  attributes.

-------------------------------------------------------------------------------

# `suse.obs.package.build_success`

~~~ python
{ 'arch'       : 'x86_64',
  'bcnt'       : '1',
  'endtime'    : '1565711121',
  'package'    : 'postgresql94-libs.SUSE_SLE-11-SP1_Update',
  'project'    : 'home:maintenance-robot:staging:198773',
  'readytime'  : '1565710960',
  'reason'     : 'new build',
  'release'    : '23.22.1',
  'repository' : 'SUSE_SLE-11-SP1_Update',
  'rev'        : '2',
  'srcmd5'     : 'bf94d2344ee7817555be02579bc54cb4',
  'starttime'  : '1565710962',
  'verifymd5'  : 'fbb732ab8159a0cbe921b21dea99caf1',
  'versrel'    : '9.4.24-23.22',
  'workerid'   : 'sheep93:1'
}
~~~

-------------------------------------------------------------------------------

## `suse.obs.package.commit`

~~~ python
{ 'files'   : 'Modified:\\n  openstack-neutron.spec\\n\\n',
  'package' : 'openstack-neutron',
  'project' : 'home:rsalevsky:branches:Devel:Cloud:7:Staging',
  'rev'     : '7',
  'user'    : 'rsalevsky'
}
~~~

## `suse.obs.package.update`

~~~ python
{ 'package' : 'openstack-neutron',
  'project' : 'home:rsalevsky:branches:Devel:Cloud:7:Staging',
  'sender'  : 'rsalevsky'
}
~~~

-------------------------------------------------------------------------------

# `suse.obs.project.comment`

~~~ python
{ 'comment_body': '<!-- openqa state=done result=accepted '
                  'revision=1565705406 -->\\n\\n\\n\\n'
                  '...'
  'commenter'   : 'sle-qam-openqa',
  'commenters'  : ['maintenance-robot', 'sle-qam-openqa'],
  'project'     : 'SUSE:Maintenance:10355'
}
~~~

-------------------------------------------------------------------------------

# `suse.obs.request.review_wanted`

~~~ python
{ 'actions'   : [{
    'action_id'      : 1045122,
    'sourcepackage'  : 'pacemaker',
    'sourceproject'  : 'home:yan_gao:branches:SUSE:SLE-12-SP4:Update',
    'sourcerevision' : 'b024370d459f901ebe6c585e9951677e',
    'sourceupdate'   : 'cleanup',
    'target_releaseproject' : 'SUSE:SLE-12-SP4:Update',
    'targetproject'  : 'SUSE:Maintenance',
    'type'           : 'maintenance_incident'
  }],
  'author'    : 'yan_gao',
  'by_group'  : 'legal-auto',
  'number'    : 198776,
  'reviewers' : [{'group_id': 2}],
  'state'     : 'new',
  'when'      : '2019-08-13T16:06:16',
  'who'       : 'yan_gao'
}
~~~

-------------------------------------------------------------------------------

# `suse.obs.request.state_change`

~~~ python
{ 'actions'     : [{
    'action_id'      : 1045194,
    'sourcepackage'  : 'python-python-engineio',
    'sourceproject'  : 'home:eapodaca:branches:Devel:Cloud:8',
    'sourcerevision' : '3301dbe602f1aefb16c99c3f9a7033ef',
    'targetpackage'  : 'python-python-engineio',
    'targetproject'  : 'Devel:Cloud:8:Staging',
    'type'           : 'submit'
  }],
  'author'      : 'eapodaca',
  'comment'     : 'superseded by 198792',
  'description' : 'Add patch CVE-2019-13611.patch (SOC-9989)',
  'number'      : 198789,
  'oldstate'    : 'review',
  'state'       : 'superseded',
  'when'        : '2019-08-13T19:44:45',
  'who'         : 'eapodaca'
}
~~~
